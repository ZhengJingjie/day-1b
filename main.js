//created on 101016

//load express
var express = require("express");
//create an instance of the express application
var app = express();

//serve file from public directory
//__dirname is the absolute path of the application directory
app.use(express.static(__dirname + "/public"));

//Start the web server on port 3000
app.listen(3000, function () {
    console.info("Web Server started on port 3000")
})